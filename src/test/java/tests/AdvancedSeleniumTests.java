package tests;

import Utils.OtherUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static Utils.SeleniumUtils.waitForGenericElement;

public class AdvancedSeleniumTests extends BaseUITest {

    @Parameters({"searchKeyword"})
    @Test
    public void browserTest(String searchKeyword) throws InterruptedException {
        driver.get("https://google.com");
        test = extent.createTest("Browser Test");
        logInfoStatus("Search for element:" + searchKeyword);
        driver.switchTo().frame(0);
        waitForGenericElement(driver, By.id("introAgreeButton"),15).click();
        WebElement input = driver.findElement(By.name("q"));
        input.sendKeys(searchKeyword);
//      press ESC button from keybord
        Thread.sleep(2000);
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ENTER).build().perform();
        logInfoStatus("Test End");
    }

    @Test
    public void cookieTest() {
        test = extent.createTest("Cookie Test Google");
        driver.get("https://google.com");
        logInfoStatus("Open https://google.com");
        OtherUtils.printCookies(test, driver);
        try {
            OtherUtils.printCookie(test, driver.manage().getCookieNamed("CONSENT1"));
        } catch (Exception e) {
            System.out.println("Cookie not found !!!");
        }
        Cookie cookie = new Cookie("myCookie", "cookie123");
        driver.manage().addCookie(cookie);
        OtherUtils.printCookies(test, driver);
        driver.manage().deleteCookieNamed("CONSENT");
        driver.manage().deleteCookieNamed("myCookie");
        OtherUtils.printCookies(test, driver);
        driver.manage().deleteAllCookies();
        OtherUtils.printCookies(test, driver);
    }

    @Test
    public void cookieCheckTest() {
        test = extent.createTest("Cookie Check Test");
        logInfoStatus("Try to open url:" + hostname + "/stubs/cookie.html");
        driver.get(hostname + "/stubs/cookie.html");
        OtherUtils.printCookies(test, driver);
        WebElement setCookie = driver.findElement(By.id("set-cookie"));
        setCookie.click();
        OtherUtils.printCookies(test, driver);
    }

    @Test
    public void screenshotTest() {
        driver.get("https://google.com");
        OtherUtils.takeScreenShot(driver);
    }

    @Test
    public void alertTests() {
//        driver.get(hostname + "/stubs/alert.html");
        driver.get("https://web-stubs.herokuapp.com/#/alert");
        WebElement alertButton = driver.findElement(By.id("alert-trigger"));
        WebElement confirmButton = driver.findElement(By.id("confirm-trigger"));
        WebElement promptButton = driver.findElement(By.id("prompt-trigger"));
        alertButton.click();
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.accept();
        confirmButton.click();
        alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.dismiss();
        promptButton.click();
        alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.sendKeys("aaaaaaa");
        alert.accept();
    }

    @Test
    public void hoverTest() {
        driver.get(hostname + "/stubs/hover.html");
        WebElement hoverButton = driver.findElement(By.xpath("/html/body/div/div/div[2]/div/button"));
        Actions actions = new Actions(driver);
//        actions = actions.moveToElement(hoverButton);
//        Action action = actions.build();
//        action.perform();
        actions.moveToElement(hoverButton).build().perform();
        WebElement itemMenu = driver.findElement(By.name("item 1"));
        System.out.println(itemMenu.getText());
        itemMenu.click();
    }

    @Test
    public void staleTest() {
        driver.get(hostname + "/stubs/stale.html");
        for (int i = 0; i < 20; i++) {
            WebElement staleButton = driver.findElement(By.id("stale-button"));
            staleButton.click();
        }
    }

    @Test
    public void clickStealerTest() {
        driver.get(hostname + "/stubs/thieves.html");
        WebElement button = driver.findElement(By.id("the_button"));
        button.click();
        Actions actions = new Actions(driver);
        // This did not work !
        //actions.sendKeys(Keys.ESCAPE).build().perform();
        WebElement closeModal = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[3]/button"));
        // This worked for me !!
        //actions.click(closeModal).build().perform();
        WebElement closeModalX = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[1]/button"));
        // Did not work !
        //actions.click(closeModalX).build().perform();
        WebElement modalText = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[2]/p"));
        System.out.println(modalText.getText());
        // This worked for me !!
        actions.moveToElement(modalText).click(modalText).sendKeys(Keys.ESCAPE).build().perform();
        //actions.click(modalText).sendKeys(Keys.ESCAPE).build().perform();

        button.click();

    }

    @Test
    public void checkBoxTest() {
        driver.get(hostname + "/stubs/thieves.html");
        WebElement checkbox = driver.findElement(By.id("the_checkbox"));
        WebElement checkboxText = driver.findElement(By.xpath("//*[@id=\"div2\"]/label/span"));
        // Checkboxes sometimes need actions in order to be clicked
        //checkbox.click();
        Actions actions = new Actions(driver);
        actions.click(checkbox).build().perform();
        System.out.println(checkboxText.getText());
    }

}
