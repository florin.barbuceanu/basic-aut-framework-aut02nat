package tests;

import Utils.OtherUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CookieHwkTest extends BaseUITest{

    // This method is used to check if a cookie value is present from the list of all the browser cookies, can be moved to utils
    private static boolean containsCookieValue(Set<Cookie> cookies, String value) {
        for (Cookie c: cookies) {
            if (c.getValue().equals(value))
                return true;
        }
        return false;
    }

    @Test
    public void cookieCheckTest() {
        test = extent.createTest("Cookie Check Test");
        logInfoStatus("Try to open url:" + hostname + "/stubs/cookie.html");
        driver.get(hostname + "/stubs/cookie.html");
        WebElement setCookieButton = driver.findElement(By.id("set-cookie"));
        setCookieButton.click();
        WebElement pageCookieValue = driver.findElement(By.id("cookie-value"));
        // Check if the cookie created is in the browser cookies
        Assert.assertTrue(containsCookieValue(driver.manage().getCookies(), pageCookieValue.getText()));
        // Check if there is at least one cookie present in the driver
        Assert.assertFalse(driver.manage().getCookies().isEmpty());
        WebElement deleteCookieButton = driver.findElement(By.id("delete-cookie"));
        deleteCookieButton.click();
        // Check that there are no existing cookies
        Assert.assertTrue(driver.manage().getCookies().isEmpty());
        pageCookieValue = driver.findElement(By.id("cookie-value"));
        // Check that the text on the page has disappeared after the cookie was deleted
        Assert.assertEquals(pageCookieValue.getText(), "");
    }

}
